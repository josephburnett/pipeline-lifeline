package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	svg "github.com/ajstarks/svgo"
	"github.com/aybabtme/uniplot/histogram"
)

type job struct {
	pipelineID     int
	jobID          int
	instance       string
	result         string
	duration       time.Duration
	queuedDuration time.Duration
	createdAt      time.Time
	startedAt      time.Time
	finishedAt     time.Time
	runner         string
	runnerCommit   string
	lifelineIndex  int
}

func main() {
	pipelineID := os.Getenv("PIPELINE_ID")
	if pipelineID == "" {
		panic("set PIPELINE_ID")
	}
	infile := fmt.Sprintf("pipeline-data-%v", pipelineID)
	b, err := os.ReadFile(infile)
	if err != nil {
		panic(err)
	}
	lines := strings.Split(string(b), "\n")
	jobs := []*job{}
	for _, line := range lines {
		fields := strings.Fields(line)
		if len(fields) != 10 {
			if len(fields) == 0 {
				continue
			}
			panic(fmt.Sprintf("need 10 fields: %q (%v)", line, len(fields)))
		}
		j := &job{}
		j.pipelineID, err = strconv.Atoi(fields[0])
		if err != nil {
			panic(err)
		}
		j.jobID, err = strconv.Atoi(fields[1])
		if err != nil {
			panic(err)
		}
		j.instance = fields[2]
		j.result = fields[3]
		j.duration, err = time.ParseDuration(fields[4] + "s")
		if err != nil {
			panic(err)
		}
		j.queuedDuration, err = time.ParseDuration(fields[5] + "s")
		if err != nil {
			panic(err)
		}
		tryParse := func(field string) time.Time {
			t, err := time.Parse("2006-01-02T15:04:05.000Z", field)
			if err != nil {
				t, err = time.Parse("2006-01-02T15:04:05.00Z", field) // sometimes there is one missing digit ¯\_(ツ)_/¯
				if err != nil {
					t, err = time.Parse("2006-01-02T15:04:05Z", field) // sometimes all of them ¯\_(ツ)_/¯
					if err != nil {
						panic(err)
					}
				}
			}
			return t
		}
		j.createdAt = tryParse(fields[6])
		j.startedAt = tryParse(fields[7])
		j.finishedAt = tryParse(fields[8])
		j.runnerCommit = fields[9]
		jobs = append(jobs, j)
	}
	if len(jobs) == 0 {
		panic("no jobs")
	}
	instanceLifelines := map[string]int{}
	for _, j := range jobs {
		lifelineIndex, _ := instanceLifelines[j.instance]
		j.lifelineIndex = lifelineIndex
		instanceLifelines[j.instance]++
	}
	instancesSet := map[string]struct{}{}
	for _, j := range jobs {
		instancesSet[j.instance] = struct{}{}
	}
	instances := []string{}
	for i := range instancesSet {
		instances = append(instances, i)
	}
	sort.Strings(instances)
	instanceOrder := map[string]int{}
	for i, instance := range instances {
		instanceOrder[instance] = i
	}

	beginning := jobs[0].startedAt
	end := jobs[0].finishedAt
	for _, j := range jobs {
		if j.startedAt.Before(beginning) {
			beginning = j.startedAt
		}
		if j.finishedAt.After(end) {
			end = j.finishedAt
		}
	}

	width := float64(1200)
	height := float64(len(jobs) * 30)

	xOffset := float64(beginning.UnixNano()) / 1000000000
	xRatio := width / (float64(end.UnixNano())/1000000000 - xOffset)
	yRatio := height / float64(len(instances))

	canvasBuf := &bytes.Buffer{}
	canvas := svg.New(canvasBuf)
	canvas.Start(int(width+400), int(height)+50)
	for i, instance := range instances {
		canvas.Text(20, int(float64(i)*yRatio)+60, instance)
	}
	for _, j := range jobs {
		lifelineOffset := (j.lifelineIndex % 2) * 20
		y := int(float64(instanceOrder[j.instance])*yRatio) + 40 + lifelineOffset
		x1 := 200 + int((float64(j.startedAt.UnixNano())/1000000000-xOffset)*xRatio)
		x2 := 200 + int((float64(j.finishedAt.UnixNano())/1000000000-xOffset)*xRatio)
		strokeWidth := int(height / float64(len(instances)) * 0.1)
		canvas.Line(x1, y, x2, y, fmt.Sprintf("fill:none;stroke:grey;stroke-width:%v;opacity:0.5", strokeWidth))
		jobLabel := fmt.Sprintf("job: %v time: %vs", strconv.Itoa(j.jobID), strconv.Itoa(int(j.duration.Seconds())))
		canvas.Text(x2+10, y+5, jobLabel, "opacity:0.5")
	}
	canvas.End()
	err = ioutil.WriteFile(fmt.Sprintf("pipeline-lifeline-%v.svg", pipelineID), canvasBuf.Bytes(), 0644)
	if err != nil {
		panic(err)
	}

	data := []float64{}
	for _, j := range jobs {
		data = append(data, float64(j.duration.Seconds()))
	}
	hist := histogram.Hist(10, data)
	histBuf := &bytes.Buffer{}
	histogram.Fprint(histBuf, hist, histogram.Linear(20))
	err = ioutil.WriteFile(fmt.Sprintf("pipeline-histogram-%v.txt", pipelineID), histBuf.Bytes(), 0644)
	if err != nil {
		panic(err)
	}
}
