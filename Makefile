# Usage:
#  export REPO=gitlab-org/ci-cd/tests/saas-runners-tests/macos-platform/XcodeBenchmark
#  make run-pipeline
#  export PIPELINE_ID=<id>
#  make collect-data
#  make analyze-data

.PHONY :
collect-data : require-pipeline require-repo
	./collect-data.sh

.PHONY :
analyze-data : require-pipeline require-repo
	go run lifeline.go

.PHONY :
run-pipeline : require-repo
	echo "Started pipeline " $$(glab ci run --repo=${REPO} --branch jburnett/mac-perf | awk '{ print $$4 }')

.PHONY :
run-pipeline-staging : require-repo
	echo "Started pipeline " $$(glab ci run --repo=${REPO} --branch jburnett/mac-perf-staging | awk '{ print $$4 }')

.PHONY :
require-pipeline :
ifndef PIPELINE_ID
	$(error set PIPELINE_ID)
endif

.PHONY :
require-repo :
ifndef REPO
	$(error set REPO)
endif
