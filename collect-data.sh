#!/bin/bash

set -e
set -u

echo "Evaluating $PIPELINE_ID"

OUTFILE=pipeline-data-$PIPELINE_ID

while true
do
    PIPELINE_STATUS=$(glab -R $REPO ci get -p $PIPELINE_ID -o json | jq --raw-output '.status')
    if [[ "$PIPELINE_STATUS" == "success" || "$PIPELINE_STATUS" == "failed" ]]
    then
	break
    fi
    echo "Waiting for pipeline $PIPELINE_ID to finish with 'success' or 'failed'"
    sleep 60
done

PIPELINE=$(glab -R $REPO ci get -p $PIPELINE_ID -o json)
JOB_IDS=$(echo "$PIPELINE" | jq '.jobs[].id')
for ID in $JOB_IDS
do
    STATUS=$(echo "$PIPELINE" | jq ".jobs[] | select(.id==$ID) | .status")
    DURATION=$(echo "$PIPELINE" | jq ".jobs[] | select(.id==$ID) | .duration")
    QUEUED_DURATION=$(echo "$PIPELINE" | jq ".jobs[] | select(.id==$ID) | .queued_duration")
    CREATED_AT=$(echo "$PIPELINE" | jq --raw-output ".jobs[] | select(.id==$ID) | .created_at")
    STARTED_AT=$(echo "$PIPELINE" | jq --raw-output ".jobs[] | select(.id==$ID) | .started_at")
    FINISHED_AT=$(echo "$PIPELINE" | jq --raw-output ".jobs[] | select(.id==$ID) | .finished_at")

    # Unfortunately we can't determine which instance ran our job through the GitLab API.
    # So we have to scrape the job logs.
    TRACE=$(glab -R $REPO ci trace $ID)
    INSTANCE=$(echo "$TRACE" | awk 'match($0, /Dialing instance (i\-[[:alnum:]]+)/, a) { print a[1] }' | uniq)
    RUNNER_COMMIT=$(echo "$TRACE" | awk 'match($0, /(\([[:alnum:]]{8}\))/, a) { print a[1] }' | uniq)

    JOB_LINE="$PIPELINE_ID $ID $INSTANCE $STATUS $DURATION $QUEUED_DURATION $CREATED_AT $STARTED_AT $FINISHED_AT $RUNNER_COMMIT"
    echo $JOB_LINE
    echo $JOB_LINE >> $OUTFILE
done
